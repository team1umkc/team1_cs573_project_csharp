﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using System.Xml.Serialization;
using System.IO;
using System.Xml;

namespace AwsDynamoDBDataLoader
{
    [DynamoDBTable("auditTable")]
    public class auditTable
    {
        [DynamoDBHashKey]
        public string msg { get; set; }
        [DynamoDBRangeKey(AttributeName = "type")]
        public string type { get; set; }

        [DynamoDBProperty("pid")]
        public string pid { get; set; }

        [DynamoDBProperty("uid")]
        public string uid { get; set; }

        [DynamoDBProperty("auid")]
        public string auid { get; set; }

        [DynamoDBProperty("ses")]
        public string ses { get; set; }

        [DynamoDBProperty("message")]
        public string message { get; set; }   
    }
}
