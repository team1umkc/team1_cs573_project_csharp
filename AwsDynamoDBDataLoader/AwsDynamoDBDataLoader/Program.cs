﻿using System;
using System.IO;
using System.Text.RegularExpressions;
using System.Security.Cryptography;

using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;

using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using Amazon.S3.Encryption;

namespace AwsDynamoDBDataLoader
{
    public partial class Program
    {
        //this is our bucket in Amazon S3
        readonly static string BUCKET_NAME = "bucketkc";       
        readonly static string LOGFILE_PATTERN = @"type=(?<type>[^\s]*)\s+msg=(?<msg>[^\s]*)\s+pid=(?<pid>[^\s]*)\s+uid=(?<uid>[^\s]*)\s+auid=(?<auid>[^\s]*)\s+ses=(?<ses>[^\s]*)\s+msg=(?<message>[^\n]*)\s+";
        readonly static string AES_EncryptionKey = "CtfzrNQpd0idpdIzT1dMXah6ubTgmV3Qf2PlGBVOeaY=";//we are using one [constant] secret key for now. 

        public static void Main(string[] args)
        {

            

            /*
            Aes encAlgorithm = Aes.Create();
            encAlgorithm.KeySize = 256;
            encAlgorithm.GenerateKey();
            string secretKey = Convert.ToBase64String(encAlgorithm.Key);
            */

            //load each file into memory, parse it, then load into DynamoDB
            try
            {
                AmazonS3Client clientS3 = new AmazonS3Client();

                S3DirectoryInfo s3Directory = new S3DirectoryInfo(clientS3, BUCKET_NAME);

                S3FileInfo[] objects = s3Directory.GetFiles();

                foreach (S3FileInfo s3Object in objects)
                {
                    Console.WriteLine("\r\n********loading file****************");
                    LoadToDynamoDB(s3Object);

                    Console.WriteLine("\r\n********Encrypting and Moving file****************");
                    EncryptAndMove(s3Object.Name, AES_EncryptionKey);
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex.Message + Environment.NewLine + ex.StackTrace);
            }
           
        }

        private static void LoadToDynamoDB(Amazon.S3.IO.S3FileInfo s3Object)
        {
            AmazonDynamoDBClient clientDB = new AmazonDynamoDBClient();
            DynamoDBContext dbContext = new DynamoDBContext(clientDB);

            TextReader reader = s3Object.OpenText();

            String fileContent = reader.ReadToEnd();

            Console.WriteLine("File read from S3 Successful!");

            Console.WriteLine("Parsing file...");

            var matches = Regex.Matches(fileContent, LOGFILE_PATTERN, RegexOptions.Singleline | RegexOptions.IgnoreCase);
            auditTable audit;

            Console.WriteLine("Start loading data...");
            foreach (Match m in matches)
            {
                audit = new auditTable
                {
                    msg = m.Groups["msg"].Value,
                    type = m.Groups["type"].Value,
                    pid = m.Groups["pid"].Value,
                    uid = m.Groups["uid"].Value,
                    auid = m.Groups["auid"].Value,
                    ses = m.Groups["ses"].Value,
                    message = m.Groups["message"].Value
                };

               
                dbContext.Save<auditTable>(audit);
            }

            Console.WriteLine(string.Format("Loading {0} rows successful!", matches.Count));

            Console.WriteLine();
        }

        private static void EncryptAndMove(string sourceObjectKey, string encryptionKey)
        {
            CopyObjectRequest encRequest = new CopyObjectRequest();
            encRequest.SourceBucket = BUCKET_NAME;
            encRequest.DestinationBucket = "enc" + BUCKET_NAME;
            encRequest.SourceKey = sourceObjectKey;
            encRequest.DestinationKey = "enc" + sourceObjectKey;

            encRequest.ServerSideEncryptionCustomerMethod = ServerSideEncryptionCustomerMethod.AES256;
            encRequest.ServerSideEncryptionCustomerProvidedKey = AES_EncryptionKey;

            //prepare delete request
            DeleteObjectRequest dRequest = new DeleteObjectRequest();

            dRequest.BucketName = BUCKET_NAME;
            dRequest.Key = sourceObjectKey;
            
            IAmazonS3 s3Client = new AmazonS3Client(Amazon.RegionEndpoint.USWest2);
            
            try
            {
                s3Client.CopyObject(encRequest);

                s3Client.DeleteObject(dRequest);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }
    }
}